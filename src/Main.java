import exception.NotValidDimensionException;

public class Main {

    public static void main(String[] argvs) {
        Matrix matrix1, matrix2;
        try {
            matrix1 = new Matrix(4, 3);
            matrix1
                    .insert(0, 0, 2)
                    .insert(0, 1, 4)
                    .insert(0, 2, 8)
                    .insert(1, 0, 3)
                    .insert(1, 1, 2)
                    .insert(1, 2, 0)
                    .insert(2, 0, 5)
                    .insert(2, 1, 3)
                    .insert(2, 2, 1)
                    .insert(3, 0, 0)
                    .insert(3, 1, 1)
                    .insert(3, 2, 0);

            matrix2 = new Matrix(4, 3);
            matrix2
                    .insert(0, 0, 1)
                    .insert(0, 1, 5)
                    .insert(0, 2, 6)
                    .insert(1, 0, 13)
                    .insert(1, 1, 24)
                    .insert(1, 2, 0)
                    .insert(2, 0, 5)
                    .insert(2, 1, 13)
                    .insert(2, 2, 1)
                    .insert(3, 0, 0)
                    .insert(3, 1, 10)
                    .insert(3, 2, 0);
            System.out.println(matrix1.toString());
            //System.out.println(matrix2.transpose());
            Matrix matrix3 = matrix1.multiply(matrix2.transpose(), true);
            float[] values = {4, 5, 3};
            matrix3 = new Matrix(4, 4, values);
            //matrix2.add(matrix1, false);
            /*float row[] = matrix.extractVector("ROW", 1);
            for (int i = 0; i < row.length; i++) {
                System.out.println(" " + row[i]);
            }*/
            System.out.println(matrix3.toString());
            //System.out.println(matrix2.toString());
        } catch (NotValidDimensionException e) {
            System.err.println(e.getMessage());
            //matrix3 = new Matrix();
        }
    }

}
