import exception.NotValidDimensionException;
import exception.NotValidParameterException;

public class Matrix {

    private float[][] matrix;
    private int numRows;
    private int numColumns;

    public enum Dimension {
        ROW, COLUMN;
    }

    /**
     * CONSEGUENZA: inizializza la matrice con dimensione 1x1 mettendo i valori a 0
     */
    public Matrix(){
        this.matrix = new float[1][1];
        this.numColumns = 1;
        this.numRows = 1;
    }

    /**
     * CONSEGUENZA: Istanzia una nuova Matrix, inizializzando tutti i parametri
     * @param matrix deve essere != null
     */
    public Matrix(float[][] matrix){
        if (matrix == null) {
            throw new NullPointerException("la matrice non deve essere null");
        }
        this.matrix = matrix;
        this.numRows = matrix.length;
        this.numColumns = matrix[0].length;
    }

    /**
     * CONSEGUENZA: inizializza la matrice di dimensione rows x numColumns,
     *  tutte le celle sono inizializzate a 0, inizializza numColumns e numRows con i valori passati
     * @param numRows numero di righe della matrice, deve essere > 0
     * @param numColumns numero di colonne della matrice, deve essere > 0
     * @throws NotValidDimensionException se i valori dimensionali sono <= 0
     */
    public Matrix(int numRows, int numColumns) throws NotValidDimensionException {
        if (numRows <= 0 || numColumns <= 0) {
            throw new NotValidDimensionException("Dimensione matrice non valida. Entrambe le dimensioni devono essere interi non negativi");
        }
        this.matrix = new float[numRows][numColumns];
        this.numColumns = numColumns;
        this.numRows = numRows;
    }

    /**
     * CONSEGUENZA: inizializza la matrice di dimensione numRows x numColumns,
     *  tutte le celle sono inizializzate con i valori contenuti in rowsValues.
     *  Se la lunghezza di rowsValues è minore del numero di righe, le celle in eccesso
     *  vengono inizializzati a 0; se, al contrario, la lunghezza di rowsValues è
     *  maggiore, i valori in eccesso verrano ignorati.
     *  Inizializza numColumns e numRows con i valori passati
     * @param numRows numero di righe della matrice, deve essere > 0
     * @param numColumns numero di colonne della matrice, deve essere > 0
     * @param rowsValues valori che verranno salvati in ogni riga di this.matrix, se = null la matrice verrà inizializzata con tutti i valori a 0
     * @throws NotValidDimensionException se i valori dimensionali sono <= 0
     */
    public Matrix(int numRows, int numColumns, float[] rowsValues) throws NotValidDimensionException {
        if (numRows <= 0 || numColumns <= 0) {
            throw new NotValidDimensionException("Dimensione matrice non valida. Entrambe le dimensioni devono essere interi non negativi");
        }
        this.matrix = new float[numRows][numColumns];
        if (rowsValues != null) {
            for (int r = 0; r < numRows; r++) {
                int c = 0;
                while (c < numColumns && c < rowsValues.length) {
                    this.matrix[r][c] = rowsValues[c];
                    c++;
                }
            }
        }
        this.numColumns = numColumns;
        this.numRows = numRows;
    }

    /**
     * MODIFICA: [row][column] cambierà di valore in value, il valore verrà sovrascritto
     * @param row riga in cui salvare value, deve essere: 0 <= row < this.rows
     * @param column colonna in cui salvare value, deve essere: 0 <= column < this.numColumns
     * @param value valore che verrà salvato in [row][column] al posto del valore presente
     * @throws NotValidDimensionException se i valori dimensionali sono <= 0 o sforano rispetto
     *  alla dimensione della matrice (es se ho 5 righe e 3 colonne e chiedo [6][2])
     * @return this con this.matrix[row][column] = value
     */
    public Matrix insert(int row, int column, float value) throws NotValidDimensionException {
        if (row < 0 || row >= this.numRows || column < 0 || column >= this.numColumns) {
            throw new NotValidDimensionException("Le coordinate selezionate non sono valide. Devono essere comprese fra 0 e la dimensione della matrice");
        }
        this.matrix[row][column] = value;
        return this;
    }

    /**
     * MODIFICA: Modifica this.matrix moltiplicandola alla matrice in input, se generateNewMatrix = false;
     *      Se generateNewMatrix = true, genera una nuova istanza di Matrix con matrix = this.matrix * matrix e i valori dimensionali = this.matrix
     * @param matrix matrice con cui this.matrix effettuerà il prodotto
     * @param generateNewMatrix se true genera una nuova istanza di Matrix, se false sovrascrive this.matrix
     * @throws NotValidDimensionException se this.numRows è diverso da matrix.numColumns
     * @return Matrix: this modificato se generateNewMatrix = false, una nuova istanza se generateNewMatrix = true
     * @throws NullPointerException se matrix = null
     */
    public Matrix multiply(Matrix matrix, boolean generateNewMatrix) throws NotValidDimensionException {
        if (matrix == null) {
            throw new NullPointerException("la matrice in input non può essere null");
        }
        if (this.numColumns != matrix.getNumRows()) {
            throw new NotValidDimensionException("La prima matrice deve avere il numero di colonne uguali al numero di righe della seconda");
        }
        if (generateNewMatrix) {
            return new Matrix(
                    this.multiplyMatrices(matrix, matrix.getNumColumns(), this.numRows)
            );
        } else {
            this.matrix = this.multiplyMatrices(matrix, matrix.getNumColumns(), this.numRows);
            this.numColumns = matrix.getNumColumns();
            return this;
        }
    }

    /**
     * Metodo che calcola il prodotto fra this.matrix e matrix.matrix
     * @param matrix matrice con cui this.matrix effettuerà il prodotto
     * @param numRows numero di righe che avrà la matrice risultante
     * @param numColumns numero di colonne che avrà la matrice risultante
     * @return float[][] matrice risultato del prodotto fra this.matrix e matrix.matrix
     */
    private float[][] multiplyMatrices(Matrix matrix, int numColumns, int numRows){
        float[][] newMatrix = new float[numRows][numColumns];
        for (int r = 0; r < numRows; r++){
            for (int c = 0; c < numColumns; c++){
                try {
                    newMatrix[r][c] = this.calculateSumOfProducts(
                            this.extractVector(Dimension.ROW.toString(), r),
                            matrix.extractVector(Dimension.COLUMN.toString(), c)
                    );
                } catch (Exception e) {} // non si realizza in quanto gli indici dei for sono nel range dimensionale delle matrici
            }
        }
        return newMatrix;
    }

    /**
     * @param row riga i-sima della prima matrice
     * @param column colonna j-sima della seconda matrice
     * @return somma dei prodotti fra gli elementi della riga i-sima della prima matrice e della colonna j-sima della seconda
     */
    private float calculateSumOfProducts(float row[], float column[]){
        int sumProducts = 0;
        for (int i = 0; i < row.length; i++){
            sumProducts += row[i] * column[i];
        }
        return sumProducts;
    }

    /**
     * MODIFICA: this.matrix viene sostituita con la sua trasposta, vengono anche invertiti i valori dimensionali
     * @return this con this.matrix trasposta
     */
    public Matrix transpose() {
        float transposed[][] = new float[this.numColumns][this.numRows];
        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numColumns; c++) {
                transposed[c][r] = this.matrix[r][c];
            }
        }
        this.matrix = transposed;

        int temp = this.numColumns;
        this.numColumns = this.numRows;
        this.numRows = temp;
        return this;
    }

    /**
     * MODIFICA: Modifica this.matrix sostituendola sommandola alla matrice in input, se generateNewMatrix = false;
     *      Se generateNewMatrix = true, genera una nuova istanza di Matrix con matrix = this.matrix + matrix e i valori dimensionali uguali alle due matrici
     * @param matrix matrice con cui this.matrix effettuerà la somma
     * @param generateNewMatrix se true genera una nuova istanza di Matrix, se false sovrascrive this.matrix
     * @return Matrix: this modificato se generateNewMatrix = false, una nuova istanza se generateNewMatrix = true
     * @throws NotValidDimensionException se le matrici hanno dimensioni differenti
     * @throws NullPointerException se matrix = null
     */
    public Matrix add(Matrix matrix, boolean generateNewMatrix) throws NotValidDimensionException {
        if (matrix == null) {
            throw new NullPointerException("la matrice in input non può essere null");
        }
        if (this.numColumns != matrix.getNumColumns() || this.numRows != matrix.getNumRows()) {
            throw new NotValidDimensionException("Le due matrici devono avere le stesse dimensioni");
        }
        if (generateNewMatrix) {
            return new Matrix(
                    this.sumMatrices(this.matrix, matrix.getMatrix(), this.numRows, this.numColumns)
            );
        } else {
            this.matrix = this.sumMatrices(this.matrix, matrix.getMatrix(), this.numRows, this.numColumns);
            return this;
        }
    }

    /**
     * @param firstFloatMatrix matrice di float
     * @param secondFloatMatrix matrice di float
     * @param numRows numero di righe della nuova matrice, uguale a quelle delle matrici in input
     * @param numColumns numero di colonne della nuova matrice, uguale a quelle delle matrici in input
     * @return float[][] la nuova matrice risultato della somma di firstFloatMatrix + secondFloatMatrix
     */
    private float[][] sumMatrices(float[][] firstFloatMatrix, float[][] secondFloatMatrix, int numRows, int numColumns) {
        float[][] newMatrix = new float[numRows][numColumns];
        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numColumns; c++) {
                newMatrix[r][c] = firstFloatMatrix[r][c] + secondFloatMatrix[r][c];
            }
        }
        return newMatrix;
    }

    /**
     * MODIFICA: ogni cella della matrice verrà moltiplicata per il valore scalar,
     *  ogni valore sarà sovrascritto
     * @param scalar valore scalare con cui verranno moltiplicate tutte le celle della matrice
     * @return this con this.matrix = scalar * this.matrix
     */
    public Matrix scalarProduct(int scalar) {
        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numColumns; c++) {
                this.matrix[r][c] *= scalar;
            }
        }
        return this;
    }

    /**
     * @param dimension può assumere due possibili valori: row|column.
     *  Se row, restituisce la riga i-sima, se column restituisce la colonna i-sima
     * @param i numero della riga o colonna da restituire, 0 < i <= dimensione matrice
     * @return float[]: la riga o la colonna i-sima
     * @throws NotValidDimensionException se i < 0
     */
    public float[] extractVector(String dimension, int i) throws NotValidDimensionException, NotValidParameterException {
        if (i < 0) {
            throw new NotValidDimensionException("Il numero di riga/colonna deve essere un intero >= 0");
        }
        return this.getGetterMatrixValuesFunction(dimension, i).getValues(this.matrix, i);
    }

    /**
     *
     * @param dimension può assumere due possibili valori: row|column.
     *  Se row, restituisce la riga i-sima, se column restituisce la colonna i-sima
     * @param i numero della riga o colonna da restituire, 0 <= i < dimensione matrice
     * @return lambda funzione per prelevare i valori della colonna/riga richiesta
     * @throws NotValidDimensionException se il valore i supera il numero di righe/colonne, a seconda di cosa si vuole recuperare
     * @throws NotValidParameterException se è stato scelto un valore diverso da row/column
     */
    private MatrixValuesInterface getGetterMatrixValuesFunction(String dimension, int i) throws NotValidDimensionException, NotValidParameterException {
        MatrixValuesInterface matrixValuesInterface = null;
        if (dimension.compareTo(Dimension.COLUMN.toString()) == 0) {
            if (i > this.numColumns) {
                throw new NotValidDimensionException("Il valore della colonna deve essere < il numero di colonne della matrice");
            }
            matrixValuesInterface = (float matrix[][], int column) -> {
                float values[] = new float[this.numRows];
                for (int r = 0; r < values.length; r++) {
                    values[r] = matrix[r][column];
                }
                return values;
            };
        } else if (dimension.compareTo(Dimension.ROW.toString()) == 0) {
            if (i > this.numRows) {
                throw new NotValidDimensionException("Il valore della riga deve essere <= il numero di righe della matrice");
            }
            matrixValuesInterface = (float matrix[][], int row) -> {
                float values[] = new float[this.numColumns];
                for (int c = 0; c < values.length; c++) {
                    values[c] = matrix[row][c];
                }
                return values;
            };
        } else {
            throw new NotValidParameterException("Parametro non valido: column o row sono i valori permessi");
        }

        return matrixValuesInterface;
    }

    public float[][] getMatrix() {
        return matrix;
    }

    public Matrix setMatrix(float[][] matrix) {
        this.matrix = matrix;
        return this;
    }

    public int getNumRows() {
        return numRows;
    }

    public Matrix setNumRows(int numRows) {
        this.numRows = numRows;
        return this;
    }

    public int getNumColumns() {
        return numColumns;
    }

    public Matrix setNumColumns(int numColumns) {
        this.numColumns = numColumns;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("");
        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numColumns; c++) {
                stringBuilder.append("[" + this.matrix[r][c] + "]");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
