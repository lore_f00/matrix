package exception;

/**
 * Eccezione nel caso la dimensione della matrice/array fosse <= 0 o eventualmente
 *      i parametri sforassero la dimensione della matrice istanziata
 */
public class NotValidDimensionException extends Exception {

    public NotValidDimensionException(String message) {
        super(message);
    }

}
