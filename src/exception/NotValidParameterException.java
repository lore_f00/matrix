package exception;

/**
 * Eccezione nel caso un parametro di scelta non fosse valido
 */
public class NotValidParameterException extends Exception {

    public NotValidParameterException(String message) {
        super(message);
    }
}
