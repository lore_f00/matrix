public interface MatrixValuesInterface {

    float[] getValues(float matrix[][], int numOfColumnOrRow);

}
